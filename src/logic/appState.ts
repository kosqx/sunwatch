import { calculateSolarTimes } from 'logic/calculate';
import { AppStateType, LocationType, ActionType } from 'logic/types';
import { datePlusDayOffset } from 'logic/utils';

export const setOffsetAction = (offset: number): ActionType => {
  return { type: 'SET_OFFSET', payload: offset };
};

export const initAppState = (): AppStateType => {
  const location: LocationType = { latitude: 52.16, longitude: 21.02 };
  const times = calculateSolarTimes(new Date(), location);
  return { offset: 0, location, times };
};

const appStateSetOffset = (state: AppStateType, offset: number): AppStateType => {
  const date = datePlusDayOffset(new Date(), Math.floor(state.offset));
  const times = calculateSolarTimes(date, state.location);

  return { ...state, offset, times };
};

export const appStateReducer = (state: AppStateType, action: ActionType): AppStateType => {
  switch (action.type) {
    case 'SET_OFFSET': {
      return appStateSetOffset(state, action.payload);
    }
    default: {
      return state;
    }
  }
};
