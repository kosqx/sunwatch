import { calculateSolarTimes, stickyScroll } from 'logic/calculate';

test('calculateSolarTimes works', () => {
  const times = calculateSolarTimes(new Date('2021-02-06'), { latitude: 50, longitude: 21 });
  expect(times).toEqual({
    astronomicalDawn: 0.21738425925925925,
    astronomicalDusk: 0.7706712962962963,
    civilDawn: 0.2701851851851852,
    civilDusk: 0.7178703703703704,
    nauticalDawn: 0.24349537037037036,
    nauticalDusk: 0.7445601851851852,
    solarMidnight: 0.9940277777777777,
    solarNoon: 0.4940277777777778,
    sunrise: 0.2940162037037037,
    sunset: 0.6940277777777778,
  });
});

test('calculateSolarTimes returns NaN when some times are missing', () => {
  const times = calculateSolarTimes(new Date('2021-06-01'), { latitude: 50, longitude: 21 });
  expect(times).toEqual({
    astronomicalDawn: NaN,
    astronomicalDusk: NaN,
    civilDawn: 0.1600925925925926,
    civilDusk: 0.8889236111111111,
    nauticalDawn: 0.1180787037037037,
    nauticalDusk: 0.9309375,
    solarMidnight: 0.02451388888888889,
    solarNoon: 0.5245138888888888,
    sunrise: 0.18979166666666666,
    sunset: 0.859224537037037,
  });
});

test.each([
  [[], 0.0, 0.0],
  [[], 0.5, 0.5],
  [[], 1.0, 1.0],
  [[0.5], 0.0, 0.0],
  [[0.5], 1.0, 1.0],
  [[0.5], 0.33, 0.363],
  [[0.5], 0.44, 0.484],
  [[0.5], 0.45, 0.5],
  [[0.5], 0.46, 0.5],
  [[0.5], 0.51, 0.5],
  [[0.5], 0.54, 0.5],
  [[0.5], 0.55, 0.505],
  [[0.3, 0.7], 0.0, 0.0],
  [[0.3, 0.7], 1.0, 1.0],
  [[0.3, 0.7], 0.125, 0.15],
  [[0.3, 0.7], 0.25, 0.3],
  [[0.3, 0.7], 0.26, 0.3],
  [[0.3, 0.7], 0.33, 0.3],
  [[0.3, 0.7], 0.34, 0.308],
  [[0.3, 0.7], 0.5, 0.5],
  [[0.3, 0.7], 0.66, 0.692],
  [[0.3, 0.7], 0.67, 0.7],
  [[0.3, 0.7], 0.74, 0.7],
  [[0.3, 0.7], 0.75, 0.7],
  [[0.3, 0.7], 0.76, 0.712],
  [[0.3, 0.7], 0.875, 0.85],
])('stickyScroll(%j, %f)', (points, offset, expected) => {
  expect(stickyScroll(points, offset)).toBeCloseTo(expected);
});
