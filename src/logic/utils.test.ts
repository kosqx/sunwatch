import { datePlusDayOffset } from 'logic/utils';

test.each([
  [new Date(2020, 0, 1), 0.0, new Date(2020, 0, 1)],
  [new Date(2020, 0, 1), 1.0, new Date(2020, 0, 2)],
  [new Date(2020, 0, 1), -1.0, new Date(2019, 11, 31)],
  [new Date(2020, 0, 1), 366.0, new Date(2021, 0, 1)],
  [new Date(2020, 0, 1), 0.5, new Date(2020, 0, 1)],
  [new Date(2020, 0, 1), -0.5, new Date(2019, 11, 31)],
])('datePlusDayOffset(%j, %f)', (date, offset, expected) => {
  expect(datePlusDayOffset(date, offset)).toEqual(expected);
});
