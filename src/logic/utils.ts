export const datePlusDayOffset = (date: Date, offset: number): Date => {
  const newDate = new Date(date);
  newDate.setDate(newDate.getDate() + offset);
  return newDate;
};
