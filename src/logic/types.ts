export interface SolarTimesType {
  solarNoon: number;
  solarMidnight: number;
  sunrise: number;
  sunset: number;
  civilDawn: number;
  civilDusk: number;
  nauticalDawn: number;
  nauticalDusk: number;
  astronomicalDusk: number;
  astronomicalDawn: number;
}

export interface LocationType {
  latitude: number;
  longitude: number;
  name?: string;
}

export interface AppStateType {
  offset: number;
  location: LocationType;
  times: SolarTimesType;
}

export type ActionType = {
  type: 'SET_OFFSET';
  payload: number;
};
