import SunCalc from 'suncalc';

import { LocationType, SolarTimesType } from 'logic/types';

export function toOffset(date: Date): number {
  const seconds = date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
  return seconds / 86400.0;
}

export function calculateSolarTimes(date: Date, location: LocationType): SolarTimesType {
  const times = SunCalc.getTimes(date, location.latitude, location.longitude);
  return {
    solarNoon: toOffset(times.solarNoon),
    solarMidnight: toOffset(times.nadir),
    sunrise: toOffset(times.sunrise),
    sunset: toOffset(times.sunset),
    civilDawn: toOffset(times.dawn),
    civilDusk: toOffset(times.dusk),
    nauticalDawn: toOffset(times.nauticalDawn),
    nauticalDusk: toOffset(times.nauticalDusk),
    astronomicalDusk: toOffset(times.night),
    astronomicalDawn: toOffset(times.nightEnd),
  };
}

export function stickyScroll(points: number[], value: number, pointSize: number = 0.1): number {
  const sorted = [...points].sort();
  const total = 1.0 + sorted.length * pointSize;
  for (let i = 0; i < sorted.length; i++) {
    const pointValue = sorted[i];
    const pointLow = (pointValue + pointSize * i) / total;
    const pointHigh = (pointValue + pointSize * (i + 1)) / total;
    if (value <= pointLow) {
      return pointValue - (pointLow - value) * total;
    }
    if (value <= pointHigh) {
      return pointValue;
    }
  }
  return 1.0 - (1.0 - value) * total;
}
