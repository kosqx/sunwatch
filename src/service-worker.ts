/// <reference lib="webworker" />
/* eslint-disable no-restricted-globals */

declare interface PrecacheEntry {
  integrity?: string;
  url: string;
  revision?: string | null;
}
type PrecacheEntries = Array<PrecacheEntry | string>;

declare global {
  interface WorkerGlobalScope {
    __WB_MANIFEST: PrecacheEntries;
  }
}
declare const self: ServiceWorkerGlobalScope;

const CACHE_NAME = 'sunwatch';

function urlsFromPrecacheEntries(entries: PrecacheEntries): string[] {
  return entries.map((item) => (typeof item === 'string' ? item : item.url));
}

self.addEventListener('install', (event: ExtendableEvent) => {
  const files = urlsFromPrecacheEntries(self.__WB_MANIFEST);
  files.push('/');
  event.waitUntil(caches.open(CACHE_NAME).then((cache) => cache.addAll(files)));
});

self.addEventListener('fetch', function (event: FetchEvent) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      if (response) {
        return response;
      }
      return fetch(event.request);
    }),
  );
});

export {};
