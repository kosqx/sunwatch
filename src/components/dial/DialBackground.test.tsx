import { nightSlices, Slice } from 'components/dial/DialBackground';

function buildTimes(
  day: [number, number] = [NaN, NaN],
  civil: [number, number] = [NaN, NaN],
  nautical: [number, number] = [NaN, NaN],
  astronomical: [number, number] = [NaN, NaN],
) {
  const [sunrise, sunset] = day;
  const [civilDawn, civilDusk] = civil;
  const [nauticalDawn, nauticalDusk] = nautical;
  const [astronomicalDawn, astronomicalDusk] = astronomical;
  return {
    solarMidnight: 0.99,
    solarNoon: 0.49,
    sunrise,
    sunset,
    civilDawn,
    civilDusk,
    nauticalDawn,
    nauticalDusk,
    astronomicalDawn,
    astronomicalDusk,
  };
}

test('nightSlices works with empty data', () => {
  const times = buildTimes();
  const slices: Slice[] = [];
  expect(nightSlices(times)).toEqual(slices);
});

test('nightSlices works with up to sunrise & sunset', () => {
  const times = buildTimes([0.27, 0.73]);
  const slices: Slice[] = [{ angleFrom: 0.73, angleTo: 0.27, kind: 'civil' }];
  expect(nightSlices(times)).toEqual(slices);
});

test('nightSlices works with up to civilDawn & civilDusk', () => {
  const times = buildTimes([0.27, 0.73], [0.25, 0.75]);
  const slices: Slice[] = [
    { angleFrom: 0.25, angleTo: 0.27, kind: 'civil' },
    { angleFrom: 0.73, angleTo: 0.75, kind: 'civil' },
    { angleFrom: 0.75, angleTo: 0.25, kind: 'nautical' },
  ];
  expect(nightSlices(times)).toEqual(slices);
});

test('nightSlices works with up to nauticalDawn & nauticalDusk', () => {
  const times = buildTimes([0.27, 0.73], [0.25, 0.75], [0.23, 0.77]);
  const slices: Slice[] = [
    { angleFrom: 0.25, angleTo: 0.27, kind: 'civil' },
    { angleFrom: 0.73, angleTo: 0.75, kind: 'civil' },
    { angleFrom: 0.23, angleTo: 0.25, kind: 'nautical' },
    { angleFrom: 0.75, angleTo: 0.77, kind: 'nautical' },
    { angleFrom: 0.77, angleTo: 0.23, kind: 'astronomical' },
  ];
  expect(nightSlices(times)).toEqual(slices);
});

test('nightSlices works with full times', () => {
  const times = buildTimes([0.27, 0.73], [0.25, 0.75], [0.23, 0.77], [0.21, 0.79]);
  const slices: Slice[] = [
    { angleFrom: 0.25, angleTo: 0.27, kind: 'civil' },
    { angleFrom: 0.73, angleTo: 0.75, kind: 'civil' },
    { angleFrom: 0.23, angleTo: 0.25, kind: 'nautical' },
    { angleFrom: 0.75, angleTo: 0.77, kind: 'nautical' },
    { angleFrom: 0.21, angleTo: 0.23, kind: 'astronomical' },
    { angleFrom: 0.77, angleTo: 0.79, kind: 'astronomical' },
    { angleFrom: 0.79, angleTo: 0.21, kind: 'night' },
  ];
  expect(nightSlices(times)).toEqual(slices);
});
