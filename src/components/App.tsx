import React from 'react';

import SunScroll from 'components/SunScroll';
import { AppStateProvider } from 'components/AppStateContext';

import 'components/App.scss';

function App() {
  return (
    <div className="App">
      <AppStateProvider>
        <SunScroll />
      </AppStateProvider>
    </div>
  );
}

export default App;
