import React, { createContext, useReducer, useContext } from 'react';

import { AppStateType, ActionType } from 'logic/types';
import { initAppState, appStateReducer } from 'logic/appState';
export { setOffsetAction } from 'logic/appState';

interface AppStateContextProps {
  state: AppStateType;
  dispatch: React.Dispatch<ActionType>;
}

const AppStateContext = createContext<AppStateContextProps>({} as AppStateContextProps);

export const AppStateProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [state, dispatch] = useReducer(appStateReducer, initAppState());

  return (
    <AppStateContext.Provider value={{ state, dispatch }}>{children}</AppStateContext.Provider>
  );
};

export const useAppState = () => {
  return useContext(AppStateContext);
};
