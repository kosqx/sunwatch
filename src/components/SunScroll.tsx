import React, { useState } from 'react';

import SunDial from 'components/SunDial';
import { useAppState, setOffsetAction } from 'components/AppStateContext';

const WHEEL_MODE_PIXEL_MULTIPLIER = {
  [WheelEvent.DOM_DELTA_PIXEL]: 1,
  [WheelEvent.DOM_DELTA_LINE]: 18,
  [WheelEvent.DOM_DELTA_PAGE]: 600,
};

function SunScroll() {
  const { dispatch } = useAppState();

  const [position, setPosition] = useState<number>(0.0);
  const [touchStartY, setTouchStartY] = useState<number>(0.0);
  const [touchDiffY, setTouchDiffY] = useState<number>(0.0);

  function calculate() {
    return position + touchDiffY * 20;
  }

  function handleWheel(event: React.WheelEvent<HTMLDivElement>) {
    const multiplier = WHEEL_MODE_PIXEL_MULTIPLIER[event.deltaMode];
    setPosition(position + event.deltaY * multiplier);
    dispatch(setOffsetAction(calculate() / 5300));
  }

  function handleTouchStart(event: React.TouchEvent<HTMLDivElement>) {
    setTouchStartY(event.touches[0].screenY);
    dispatch(setOffsetAction(calculate() / 5300));
  }

  function handleTouchMove(event: React.TouchEvent<HTMLDivElement>) {
    setTouchDiffY(event.touches[0].screenY - touchStartY);
    dispatch(setOffsetAction(calculate() / 5300));
  }

  function handleTouchEnd(event: React.TouchEvent<HTMLDivElement>) {
    setPosition(calculate());
    setTouchStartY(0);
    setTouchDiffY(0);
    dispatch(setOffsetAction(calculate() / 5300));
  }

  return (
    <div
      onWheel={handleWheel}
      onTouchMove={handleTouchMove}
      onTouchStart={handleTouchStart}
      onTouchEnd={handleTouchEnd}
    >
      <SunDial></SunDial>
    </div>
  );
}

export default SunScroll;
