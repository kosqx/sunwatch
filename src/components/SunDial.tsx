import React from 'react';

import DialBackground from 'components/dial/DialBackground';
import DialOffsetText from 'components/dial/DialOffsetText';
import DialPoints from 'components/dial/DialPoints';
import DialRing from 'components/dial/DialRing';

function SunDial(props: {}) {
  return (
    <svg
      viewBox="0 0 1000 1000"
      width="400"
      height="400"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <DialBackground></DialBackground>
      <DialRing></DialRing>
      <DialPoints></DialPoints>
      <DialOffsetText></DialOffsetText>
    </svg>
  );
}

export default SunDial;
